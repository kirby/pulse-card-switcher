# PulseAudio Card Switcher (Prototype)

 * * * * *
**I'm releasing this as a prototype!**

It does what I need it to, but it's far from polished, and for a proper release I would rewrite it from scratch.
 * * * * *

## What does it do?

It lets you switch between different PulseAudio devices ("sound cards"), and their profiles (highest priority profiles only for now).

## Screenshot

![Screenshot showing 8 devices in "Sound Card Switcher" software: 6 speakers, 2 microphones, one of each is active, as noted by a colorful circle.](pa_card_switcher_screenshot.png)

### What can I use it for?

Say you have several sound cards, for example (1) your mainboard's audio out connected to a set of speakers, (2) a USB microphone, and (3) the transceiver for a wireless headset. There is also (4) the HDMI out of your video card, that you do not use because it is connected to PC monitor without speakers.  
If you want to switch from the speakers to the headset, but use the USB microphone, not the headset's, you could navigate through the sound settings or `pulsemixer` (always skipping past the HDMI audio you don't want or need), or you could launch this software and do it with a few keystrokes.

Please see the accompanying gif ([pa_card_switcher_recording.gif](pa_card_switcher_recording.gif) 928 KiB) for a demo with an excessive number of sound cards.

## How to use it

* To select a device as the exclusive in- or output, press `Shift` + the device's number (`1`-`9`), or select it in the GUI and press `Enter`, or double-click on it with the mouse.
* To toggle a device on or off press the device's number (`1`-`9`), or select it in the GUI and press `Space`.
* To disable all outputs, press `S` (for Speakers)
* To disable all inputs, press `M` (for Microphone)
* To reload the list (if you plugged in a new device, or removed one), press `R`.
* To quit press `Escape`.

## Command Line Options
`--hide HIDE [HIDE ...]`  
Hide any devices with these terms in their name in the GUI (not case-sensitive).

`--hide-inputs [HIDE_INPUTS ...]`  
Hide input devices with these terms in their name, or use it without arguments to hide all.

`--hide-outputs [HIDE_OUTPUTS ...]`  
Hide output devices with these terms in their name, or use it without arguments to hide all.

`--ignore IGNORE [IGNORE ...]`  
Completely ignore any devices with these terms in their name, i.e. do not switch them off when switching exclusive outputs or inputs.

`--rename OLD_NAME NEW_NAME`  
Rename device from old name to new name. Can be repeated for additional devices.

## Examples
`pa_card_switcher.py --hide-inputs`  
Hides all inputs, shows only outputs.

`pa_card_switcher.py --hide-outputs HDMI`  
Hides outputs with HDMI in their name (e.g. video cards), but still shows inputs with HDMI in their name (e.g. capture cards).

`pa_card_switcher.py --hide "absurd device"`  
Hides sound card(s) that have the string "Absurd Device" in their name, whether input or output.

`pa_card_switcher.py --ignore Vital`  
Ignores sound cards with "Vital" in their name, even when switching to an exclusive device or disabling all devices.

`pa_card_switcher.py --rename "Manufacturer Inc. Ltd. Long Device Name ACAB-42" "My headphones"`  
Renames the long and hard to remember device to *My headphones*.
    
## Notes

* All arguments to hide, ignore or rename devices are case-insensitive.
* Arguments with spaces in them need to be put in quotation marks (see example above) or will be interpreted as separate arguments.
* `--ignore` does not affect device priority, that is handled by PulseAudio. Ignored devices can still lose audio to a higher priority device.
* pa_card_switcher.py only shows the highest priority profiles for devices. To change the profile priority, see below.

## Installation

Make sure your system has all the requirements installed (see below), download the `pa_card_switcher.py` file, and then you can run it with `python3 pa_card_switcher.py`.  
You might want to bind it to a keyboard shortcut in your window manager, or add it to the application menu.  
You an also copy it to `.local/bin/` in your home directory and make it executable. Then you should be able to call it like any other program on your system (you might have to create that directory and log out and back in).

## Changing profile priorities

(Tested on Ubuntu 22.04)

Run `lsusb` and look for your card's Bus and Device number. If it's not a USB device, you can probably figure out another way to get this information.

Then run
```
udevadm info -a -p $(udevadm info -q path -n /dev/bus/usb/<BUS>/<DEVICE>)
```
to see what attributes udev has for your card. Look for `ATTR{product}`, or `ATTR{idProduct}` and `ATTR{idVendor}`.

To create a new udev rule using these, place a file called `89-pulseaudio-<YOUR-DEVICE>.rules` in `/etc/udev/rules.d/`, with the following content:
```
ACTION=="add", SUBSYSTEM=="sound", KERNEL=="card*", \
ATTRS{idVendor}=="<idVendor>", ATTRS{idProduct}=="<idProduct>", ENV{PULSE_PROFILE_SET}="<YOUR-DEVICE>.conf"
```
or
```
ACTION=="add", SUBSYSTEM=="sound", KERNEL=="card*", \
ATTRS{product}=="<product>", ENV{PULSE_PROFILE_SET}="<YOUR-DEVICE>.conf"
```
(Note the `ATTRS` with an `S` at the end, *not* `ATTR` without the `S`!)

Next place a file called `<YOUR-DEVICE>.conf` in `/usr/share/pulseaudio/alsa-mixer/profile-sets/`. Copy its contents from `default.conf` or another config file and then simply scroll down to the profile (called "Mapping" here) you want to change, and edit its `priority` to a number higher than the others (e.g. 20).

## Requirements 

(These are the versions I was working with, lower version *might* work, but were not tested. Higher versions *should* work (for a while), but that also has not been tested.)

* PulseAudio 15.99
* python 3.10 & wxPython 4.0
* [Fork Awesome font](https://forkaweso.me/) 1.2.0 ([direct download](https://github.com/ForkAwesome/Fork-Awesome/raw/master/fonts/forkawesome-webfont.ttf))

Optional:

* A color emoji font (if you want colorful icons in the rightmost column)
