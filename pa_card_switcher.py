#!/usr/bin/env python3

import subprocess
import json
import wx
import argparse


class GuiWindow(wx.Frame):
    def __init__(self, ignore_list, rename_list):
        super().__init__(None, title="Sound Card Switcher", size=(700, 360))
        self.Bind(wx.EVT_SIZE, self.on_resize)
        self.mapping = {}
        self.cards = {}
        self.num_outputs = 0
        self.ignore_list = ignore_list
        self.rename_list = rename_list if rename_list else []  # empty list instead of None
        self.list = wx.ListCtrl(self, style=wx.LC_REPORT | wx.LC_NO_HEADER | wx.LC_SINGLE_SEL)
        self.list.SetFont(wx.Font(wx.FontInfo(10).FaceName("forkawesome")))
        self.list.AppendColumn("k", width=20, format=wx.LIST_FORMAT_CENTER)  # k = key
        self.list.AppendColumn("t", width=20, format=wx.LIST_FORMAT_LEFT)  # t = type (speaker or mic)
        self.list.AppendColumn("name", width=300)  # Device name
        self.list.AppendColumn("profile", width=235, format=wx.LIST_FORMAT_CENTER)  # Profile name
        self.list.AppendColumn("s", width=25, format=wx.LIST_FORMAT_RIGHT)  # s = status
        self.small_cols_width = self.list.GetColumnWidth(0) + self.list.GetColumnWidth(1) + self.list.GetColumnWidth(4)
        self.populate_list()
        self.list.Select(0)
        self.list.Bind(wx.EVT_KEY_DOWN, self.on_key_down)
        self.list.Bind(wx.EVT_LIST_ITEM_ACTIVATED, self.on_doubleclick)
        self.on_resize(0)
        self.Show(True)

    # noinspection PyUnusedLocal
    def on_resize(self, e):  # e is not used, but is required by the Bind
        x, y = self.GetClientSize()
        self.list.SetColumnWidth(2, int(((x - self.small_cols_width) / 2)) + 1)  # compensate for the rounding down
        self.list.SetColumnWidth(3, int((x - self.small_cols_width) / 2))
        self.list.SetSize(wx.DefaultCoord, wx.DefaultCoord, x, y)

    # noinspection PyUnusedLocal
    def on_doubleclick(self, e):  # e is not used, but is required by the Bind
        self.switch(self.list.GetFirstSelected())

    def on_key_down(self, e):
        key = e.GetUnicodeKey()
        if key == wx.WXK_NONE:  # let the ListCtrl take care of special keys like cursor, Home, End, PgUp, PgDn, etc.
            e.Skip()
        elif key == wx.WXK_ESCAPE:  # close the app
            self.Close()
        elif key == wx.WXK_RETURN:  # switch to selected card exclusively
            self.switch(self.list.GetFirstSelected())
        elif key == wx.WXK_SPACE:  # toggle this specific card
            self.toggle(self.list.GetFirstSelected())
        elif key == 77:  # M key; disable all microphones
            self.disable_all_inputs(self.list.GetFirstSelected())
        elif key == 83:  # S key; disable all speakers
            self.disable_all_outputs(self.list.GetFirstSelected())
        elif key == 82:  # R key; reload
            selection = self.list.GetFirstSelected()
            self.list.DeleteAllItems()
            self.populate_list()
            self.list.Select(selection)
        elif key in range(49, 58):  # numbers 1 - 9
            number = key - 49  # now 0 - 8, since ListBox is 0-indexed
            if number < self.list.GetItemCount():
                if e.ShiftDown():
                    self.switch(number)
                else:
                    self.toggle(number)
        elif key == 40:  # WTF: Shift + 9 returns "(" (ASCII 40) instead of "9", this is a workaround for a wx bug i.g.
            if e.ShiftDown():
                self.switch(8)

    def populate_list(self):
        self.cards = self.get_cards()

        num = 0
        # outputs
        for card in self.cards.values():
            if card['visible'] and (card['type'] == "output" or card['type'] == "combined"):
                self.mapping[num] = card
                num += 1
                row = list()
                # Number
                row.append(str(num) + "\u20E3" if num < 10 else "")  # Combining Enclosing Keycap
                # Icon
                if "output:" in card['current']:
                    row.append("\uf028")  # forkawesome fa-volume-up
                else:
                    row.append("\uf32f")  # forkawesome fa-volume-mute
                # Card Name
                row.append(card['name'])
                # Active Profile
                if "output:" in card['current']:
                    row.append("\u25B6 " + card['profile_name'])  # Play Button
                elif card['current'] == card['off'] or card['current'] == card['input']:
                    row.append("-")
                else:
                    row.append(card['profile_name'])
                # Color Icon, easier to see, but less meaningful. Need a colorful "crossed out microphone" emoji…
                if "output:" in card['current']:
                    row.append("\U0001F7E2")  # green circle emoji; \U0001F7E2 "Large Green Circle"
                else:
                    row.append("\u274c")  # X mark emoji; \u274c "Cross Mark"

                self.list.Append(row)

        self.num_outputs = num

        # inputs
        for card in self.cards.values():
            if card['visible'] and (card['type'] == "input" or card['type'] == "combined"):
                self.mapping[num] = card
                num += 1
                row = list()
                row.append(str(num) + "\u20E3" if num < 10 else "")  # Combining Enclosing Keycap
                if "input:" in card['current']:
                    row.append("\uf130")  # forkawesome fa-microphone
                else:
                    row.append("\uf131")  # forkawesome fa-microphone-slash
                row.append(card['name'])
                if "input:" in card['current']:
                    row.append("\u23FA " + card['profile_name'])  # Record Button
                elif card['current'] == card['off'] or card['current'] == card['output']:
                    row.append("-")
                else:
                    row.append(card['profile_name'])
                if "input:" in card['current']:
                    # row.append("\U0001F534")  # red circle emoji; \U0001F534 “Large Red Circle”"
                    row.append("\U0001F535")  # blue circle emoji; \U0001F535 “Large Blue Circle”"
                else:
                    row.append("\u274c")  # X mark emoji; \u274c "Cross Mark"

                self.list.Append(row)

    def toggle(self, selection):
        selected_card = self.mapping[selection]
        index = selected_card['index']
        if selection < self.num_outputs:  # the selection was made for an output
            if selected_card['current'] == selected_card['off']:  # it's off, turn it on!
                profile = selected_card['output']
            elif selected_card['current'] == selected_card['combined']:  # it's CURRENTLY input AND output…
                profile = selected_card['input']  # … so turn off the output
            elif selected_card['current'] == selected_card['input']:  # It's on output with an INPUT profile active…
                profile = selected_card['combined']  # …so we switch it to the combined profile!
            else:  # that leaves only that it's currently an active output, so we turn it off!
                profile = selected_card['off']
        else:  # the selection was made for an input
            if selected_card['current'] == selected_card['off']:
                profile = selected_card['input']
            elif selected_card['current'] == selected_card['combined']:
                profile = selected_card['output']
            elif selected_card['current'] == selected_card['output']:
                profile = selected_card['combined']
            else:
                profile = selected_card['off']

        subprocess.run(['pactl', 'set-card-profile', str(index), profile], check=True)
        self.list.DeleteAllItems()
        self.populate_list()
        self.list.Select(selection)

    def switch(self, selection):
        selected_card = self.mapping[selection]

        if selection < self.num_outputs:  # an output was selected
            for card in self.cards.values():
                index = card['index']
                # the selected card
                if card == selected_card and selected_card['type'] == "output":
                    profile = card['output']
                elif card == selected_card and selected_card['type'] == "combined":
                    if "input:" in card['current']:  # if it is currently active as input
                        profile = card['combined']
                    else:
                        profile = card['output']
                # other cards
                elif card['type'] == "combined" and "input:" in card['current']:
                    profile = card['input']
                elif card['type'] == "input":
                    continue  # skip inputs
                else:
                    profile = card['off']
                subprocess.run(['pactl', 'set-card-profile', str(index), profile], check=True)
                # print(index, profile)

        else:
            for card in self.cards.values():
                index = card['index']
                # the selected card
                if card == selected_card and selected_card['type'] == "input":
                    profile = card['input']
                elif card == selected_card and selected_card['type'] == "combined":
                    if "output:" in card['current']:  # if it is currently active as output
                        profile = card['combined']
                    else:
                        profile = card['input']
                # other cards
                elif card['type'] == "combined" and "output:" in card['current']:
                    profile = card['output']
                elif card['type'] == "output":
                    continue  # skip output
                else:
                    profile = card['off']
                subprocess.run(['pactl', 'set-card-profile', str(index), profile], check=True)
                # print(index, profile)

        self.list.DeleteAllItems()
        self.populate_list()
        self.list.Select(selection)

    def disable_all_inputs(self, selection):
        for card in self.cards.values():
            if "input:" in card['current']:
                index = card['index']
                if "output:" in card['current']:
                    profile = card['output']
                else:
                    profile = card['off']
                subprocess.run(['pactl', 'set-card-profile', str(index), profile], check=True)
        self.list.DeleteAllItems()
        self.populate_list()
        self.list.Select(selection)

    def disable_all_outputs(self, selection):
        for card in self.cards.values():
            if "output:" in card['current']:
                index = card['index']
                if "input:" in card['current']:
                    profile = card['input']
                else:
                    profile = card['off']
                subprocess.run(['pactl', 'set-card-profile', str(index), profile], check=True)
        self.list.DeleteAllItems()
        self.populate_list()
        self.list.Select(selection)

    def get_cards(self):
        pactl = subprocess.run(['pactl', '-f', 'json', 'list', 'cards'], capture_output=True)

        if pactl.returncode:
            print("pactl error: " + pactl.stderr.decode('utf-8'))
            exit(pactl.returncode)

        devices = json.loads(pactl.stdout)

        cards = {}

        # we don't need most of the stuff
        for device in devices:
            card = {
                'index': device['index'],
                'device_name': device['name'],
                'name': "",
                'type': "",
                'current': "",
                'profile_name': "",
                'off': "off",
                'output': None,
                'input': None,
                'combined': None,
                'visible': True
            }

            if device['properties']['device.description']:
                card['name'] = device['properties']['device.description']
            else:
                card['name'] = device['name']
            priority = {'output': 0, 'input': 0, 'combined': 0}
            for (name, profile) in device['profiles'].items():
                if "input:" in name and "output:" in name:
                    card_type = "combined"
                elif "input:" in name:
                    card_type = "input"
                elif "output:" in name:
                    card_type = "output"
                else:
                    continue  # a profile that is neither input nor output nor both, wtf is it???
                if profile['priority'] > priority[card_type]:
                    priority[card_type] = profile['priority']
                    card[card_type] = name
            if card['output']:
                card['type'] = "output"
            if card['input']:
                card['type'] = "input"
            if card['combined']:
                card['type'] = "combined"
            card['current'] = device['active_profile']
            card['profile_name'] = device['profiles'][card['current']]['description']

            if self.ignore_list['general']:
                for ignore in self.ignore_list['general']:
                    if ignore.lower() in card['name'].lower():
                        card['visible'] = False

            if self.ignore_list['inputs'] is not False:
                if len(self.ignore_list['inputs']) == 0:  # ignore all
                    if card['type'] == "combined":
                        card['type'] = "output"
                    elif card['type'] == "input":
                        card['visible'] = False
                else:
                    for ignore in self.ignore_list['inputs']:
                        if ignore.lower() in card['name'].lower():
                            if card['type'] == "combined":
                                card['type'] = "output"
                            elif card['type'] == "input":
                                card['visible'] = False

            if self.ignore_list['outputs'] is not False:
                if len(self.ignore_list['outputs']) == 0:
                    if card['type'] == "combined":
                        card['type'] = "input"
                    elif card['type'] == "output":
                        card['visible'] = False
                else:
                    for ignore in self.ignore_list['outputs']:
                        if ignore.lower() in card['name'].lower():
                            if card['type'] == "combined":
                                card['type'] = "input"
                            elif card['type'] == "output":
                                card['visible'] = False

            ignore_this = False

            if self.ignore_list['ignore']:
                for ignore in self.ignore_list['ignore']:
                    if ignore.lower() in card['name'].lower():
                        ignore_this = True

            # rename after ignoring is done
            for rename in self.rename_list:
                if card['name'].lower() == rename[0].lower():
                    card['name'] = rename[1]

            if not ignore_this:
                cards[device['name']] = card
        return cards


def main():
    parser = argparse.ArgumentParser(formatter_class=argparse.RawDescriptionHelpFormatter,
                                     description="Switch between sound cards, and their input and output profiles.",
                                     epilog="""
examples:
  %(prog)s --hide-inputs
    Hides all inputs, shows only outputs.
  %(prog)s --hide-outputs HDMI
    Hides outputs with HDMI in their name (video cards), still shows inputs with HDMI in their name (capture cards).
  %(prog)s --hide "absurd device"
    Hides sound card(s) that have the string "Absurd Device" in their name, whether input or output.
  %(prog)s --ignore Vital
    Ignores sound cards with "Vital" in their name, even when switching to an exclusive device or disabling all devices.
    
notes:
  All arguments to hide or ignore devices are case-insensitive.
  Arguments with spaces in them need to be put in quotation marks or will be interpreted as separate arguments.
  --ignore does not affect device priority, that is handled by PulseAudio. Ignored devices can still lose audio.
  %(prog)s shows descriptive device names and only the highest priority profiles for devices.
  
keyboard shortcuts:
  1-9 or Space: Toggle numbered or selected output/input (turn on or off).
  Shift + 1-9, Enter, or Doubleclick: Make numbered or selected output/input the exclusive output/input.
  S: Turn all outputs ("Speakers") off.
  M: Turn all inputs ("Microphones") off.
  R: Reload devices.
  Esc: Quit.
""")
    parser.add_argument("--hide", nargs="+",
                        help="Hide any devices with these terms in their name in the GUI.")
    parser.add_argument("--hide-inputs", nargs="*",
                        help="Hide input devices with these terms in their name, or use it without arguments to "
                             "hide all.")
    parser.add_argument("--hide-outputs", nargs="*",
                        help="Hide output devices with these terms in their name, or use it without arguments to "
                             "hide all.")
    parser.add_argument("--ignore", nargs="+",
                        help="Completely ignore any devices with these terms in their name, i.e. do not switch them "
                             "off.")
    parser.add_argument("--rename", action="append" ,nargs=2, metavar=("OLD_NAME", "NEW_NAME"),
                        help="Rename device from old name to new name. Can be repeated for additional devices.")

    args = parser.parse_args()

    ignore_list = {
        'general': args.hide if args.hide else False,
        'inputs': args.hide_inputs if args.hide_inputs is not None else False,
        'outputs': args.hide_outputs if args.hide_outputs is not None else False,
        'ignore': args.ignore if args.ignore else False
    }

    app = wx.App()
    GuiWindow(ignore_list, args.rename)
    app.MainLoop()


if __name__ == "__main__":
    main()
